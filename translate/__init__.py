
class Writer():
    def __init__(self, filepath, file):
        self.filepath = filepath
        self.file = file
    
    def joinFilepath(self):
        if len(self.filepath) > 1:
            return "".join(x + "/" for x in self.filepath[:-1]) 

    def do(self, str):
        with open(f'{self.joinFilepath()}{self.file}.asm', 'a') as writer:
            writer.write(f'{str}\n')

        

