from re import I
import re
from sys import argv
from . import Writer

class CodeWriter():

    def __init__(self, filepath, file):
        self.writer = Writer(filepath, file)
        self.file = file.split(".")[0]
        self.segments = {
            "local" : "LCL",
            "argument": "ARG",
            "that" : "THAT",
            "this" : "THIS"
        }

        self.push = {
            "constant" : self.writePushConstant,
            "local" : self.writePushSegment,
            "argument": self.writePushSegment,
            "that" : self.writePushSegment,
            "this" : self.writePushSegment,
            "temp" : self.writePushSegment,
            "pointer" : self.writePushPointer,
            "static" : self.writePushSegment
        }

        self.labelCount = 0
        self.callCount = 0
        self.returnSubCount = 0
        self.funcName = ''
        self.callCount = 0

    def writePushConstant(self, args):
        constant = args[2]
        self.writer.do(f'@{constant} // push constant {constant}')
        self.writer.do("D=A")
        self.writer.do("@SP")
        self.writer.do("A=M")
        self.writer.do("M=D")
        self.writer.do("@SP")
        self.writer.do("M=M+1")

    def writePushPointer(self, args):
        self.writer.do("@THIS" if int(args[2]) == 0 else "@THAT")
        self.writer.do("D=M")
        self.writer.do("@SP")
        self.writer.do("A=M")
        self.writer.do("M=D")
        self.writer.do("@SP")
        self.writer.do("M=M+1")

    def buildSegment(self, segment, index):
        
        index = int(index)
        if segment == "temp":
            return f"{5+int(index)}"
        if segment == "static":
             return f"{self.file}.{index}"
        sigla = self.segments.get(segment)
        if sigla != None:
            return sigla
        raise(BaseException("Segmento inválido " + segment + "."))    
        
    def writePushSegment(self, args):
        segment = args[1]
        index = args[2]
        if segment == "temp" or segment == "static":
            self.writer.do(f"@{self.buildSegment(segment, index)} // push {segment} {index}")
            self.writer.do("D=M")
            self.writer.do("@SP")
            self.writer.do("A=M")
            self.writer.do("M=D")
            self.writer.do("@SP")
            self.writer.do("M=M+1")
            return
        self.writer.do(f"@{self.buildSegment(segment, index)} // push {segment} {index}")
        self.writer.do("D=M")
        self.writer.do(f"@{index}")
        self.writer.do("A=D+A")
        self.writer.do("D=M")
        self.writer.do("@SP")
        self.writer.do("A=M")
        self.writer.do("M=D")
        self.writer.do("@SP")
        self.writer.do("M=M+1")
    
    def writePush(self, args):
        self.push[args[1]](args)

    def writePop(self, args):
        segment = args[1]
        index = args[2]
        if segment == "pointer":
            self.writer.do("@SP")
            self.writer.do("AM=M-1")
            self.writer.do("D=M")
            self.writer.do("@THIS" if int(args[2]) == 0 else "@THAT")
            self.writer.do("M=D")
            return
        if segment == "static" or segment == "temp":
            self.writer.do(f"@SP // pop {segment} {index}")
            self.writer.do("M=M-1")
            self.writer.do("A=M")
            self.writer.do("D=M")
            self.writer.do(f"@{self.buildSegment(segment, index)}")
            self.writer.do("M=D")
            return
        if segment == "local" or segment == "argument" or segment == "this" or segment == "that":
            self.writer.do(f"@{self.buildSegment(segment, index)} // pop {segment} {index}")
            self.writer.do("D=M")
            self.writer.do(f"@{index}")
            self.writer.do("D=D+A")
            self.writer.do("@R13")
            self.writer.do("M=D")
            self.writer.do("@SP")
            self.writer.do("M=M-1")
            self.writer.do("A=M")
            self.writer.do("D=M")
            self.writer.do("@R13")
            self.writer.do("A=M")
            self.writer.do("M=D")

            return
        raise(BaseException(f"Ocorreu um erro no push {segment}"))

    def writeBinaryArithmetic(self, args):
        self.writer.do(f"@SP // {args[0]}")
        self.writer.do("AM=M-1")
        self.writer.do("D=M")
        self.writer.do("A=A-1")

    def writeAdd(self, args):
        self.writeBinaryArithmetic(args)
        self.writer.do("M=D+M")

    def writeSub(self, args):
        self.writeBinaryArithmetic(args)
        self.writer.do("M=M-D")
    
    def writeAnd(self, args):
        self.writeBinaryArithmetic(args)
        self.writer.do("M=D&M")
    
    def writeOr(self, args):
        self.writeBinaryArithmetic(args)
        self.writer.do("M=D|M")

    def writeUnary(self, args):
        self.writer.do(f"@SP //{args[0]}")
        self.writer.do("A=M")
        self.writer.do("A=A-1")

    def writeNegative(self, args):
        self.writeUnary(args)
        self.writer.do("M=-M")

    def writeNot(self, args):
        self.writeUnary(args)
        self.writer.do("M=!M")
    
    def writeEq(self, args):
        label = f"JEQ_{self.file}_{self.labelCount}"
        self.writer.do("@SP // eq")
        self.writer.do("AM=M-1")
        self.writer.do("D=M")
        self.writer.do("@SP")
        self.writer.do("AM=M-1")
        self.writer.do("D=M-D")
        self.writer.do("@" + label)
        self.writer.do("D;JEQ")
        self.writer.do("D=1")
        self.writer.do("(" + label + ")")
        self.writer.do("D=D-1")
        self.writer.do("@SP")
        self.writer.do("A=M")
        self.writer.do("M=D")
        self.writer.do("@SP")
        self.writer.do("M=M+1")

        self.labelCount +=1 

    def writeGt(self, args):
        labelTrue = f"JGT_TRUE_{self.file}_{self.labelCount}"
        labelFalse = f"JGT_FALSE_{self.file}_{self.labelCount}"
        
        self.writer.do("@SP // gt")
        self.writer.do("AM=M-1")
        self.writer.do("D=M")
        self.writer.do("@SP")
        self.writer.do("AM=M-1")
        self.writer.do("D=M-D")
        self.writer.do("@" + labelTrue)
        self.writer.do("D;JGT")
        self.writer.do("D=0")
        self.writer.do("@" + labelFalse)
        self.writer.do("0;JMP")
        self.writer.do("(" + labelTrue + ")")
        self.writer.do("D=-1")
        self.writer.do("(" + labelFalse + ")")
        self.writer.do("@SP")
        self.writer.do("A=M")
        self.writer.do("M=D")
        self.writer.do("@SP")
        self.writer.do("M=M + 1")

        self.labelCount +=1 
    
    def writeLt(self, args):
        labelTrue = f"JLT_TRUE_{self.file}_{self.labelCount}"
        labelFalse = f"JLT_FALSE_{self.file}_{self.labelCount}"
        
        self.writer.do("@SP // gt")
        self.writer.do("AM=M-1")
        self.writer.do("D=M")
        self.writer.do("@SP")
        self.writer.do("AM=M-1")
        self.writer.do("D=M-D")
        self.writer.do("@" + labelTrue)
        self.writer.do("D;JLT")
        self.writer.do("D=0")
        self.writer.do("@" + labelFalse)
        self.writer.do("0;JMP")
        self.writer.do("(" + labelTrue + ")")
        self.writer.do("D=-1")
        self.writer.do("(" + labelFalse + ")")
        self.writer.do("@SP")
        self.writer.do("A=M")
        self.writer.do("M=D")
        self.writer.do("@SP")
        self.writer.do("M=M+1")

        self.labelCount +=1 
    
    def writeFunction(self, args):
        funcName = args[1]
        locals = args[2]
        loopLabel =  f"{funcName}_INIT_LOCALS_LOOP"
        loopEndLabel = f"{funcName}_INIT_LOCALS_END"

        self.funcName = funcName

        self.writer.do(f"({funcName}) // function - inicializa as variáveis locais")
        self.writer.do(f"@{locals}")
        self.writer.do("D=A")
        self.writer.do("@R13") 
        self.writer.do("M=D")
        self.writer.do(f"({loopLabel})")
        self.writer.do(f"@{loopEndLabel}")
        self.writer.do("D;JEQ")
        self.writer.do("@0")
        self.writer.do("D=A")
        self.writer.do("@SP")
        self.writer.do("A=M")
        self.writer.do("M=D")
        self.writer.do("@SP")
        self.writer.do("M=M+1")
        self.writer.do("@R13")
        self.writer.do("MD=M-1")
        self.writer.do(f"@{loopEndLabel}")
        self.writer.do("0;JMP")
        self.writer.do(f"({loopLabel})")

    def writeReturn(self, args):
        returnAddress = f"$RET{self.returnSubCount}"
        self.writer.do(f"@{returnAddress} // {args[0]}") 
        self.writer.do("D=A")
        self.writer.do("@$RETURN$")
        self.writer.do("0;JMP")
        self.writer.do(f"({returnAddress})")
        self.returnSubCount += 1

    def writeCall(self, args):
        funcName = args[1]
        numArgs = args[2]

        returnAddress = f"{funcName}_RETURN_{self.callCount}"
        self.callCount += 1

        self.writer.do(f"@{returnAddress} // call {funcName} {numArgs}")
        self.writer.do("D=A")
        self.writer.do("@SP")
        self.writer.do("A=M")
        self.writer.do("M=D")
        self.writer.do("@SP")
        self.writer.do("M=M+1")

        returnFrame = f"$RET{self.returnSubCount}"
        self.writer.do(f"@{returnFrame}")
        self.writer.do("D=A")
        self.writer.do("@$FRAME$")
        self.writer.do("0;JMP")
        self.writer.do(f"({returnFrame})")
        self.returnSubCount += 1

        self.writer.do(f"@{numArgs}")
        self.writer.do("D=A")
        self.writer.do("@5")
        self.writer.do("D=D+A")
        self.writer.do("@SP")
        self.writer.do("D=M-D")
        self.writer.do("@ARG")
        self.writer.do("M=D")

        self.writer.do("@SP") 
        self.writer.do("D=M")
        self.writer.do("@LCL")
        self.writer.do("M=D")
        self.writer.do(f"@{funcName}" )
        self.writer.do("0;JMP")

        self.writer.do(f"({returnAddress})") 

    def writeIf(self, args):
        label = args[1]

        self.writer.do("@SP")
        self.writer.do("AM=M-1")
        self.writer.do("D=M")
        self.writer.do("M=0")
        self.writer.do(f"@{self.funcName}${label}")
        self.writer.do("D;JNE")

    def writeGoto(self, args):
        label = args[1]
        self.writer.do(f"@{self.funcName}${label} // goto {label}")
        self.writer.do("0;JMP")
    
    def writeLabel(self, args):
        label = args[1]
        self.writer.do(f"({self.funcName}${label}) // label {label}")

