from translate import Writer
from translate import code_writer
from translate.code_writer import CodeWriter
import sys
import re

def getFilePath(filepath):
    path = filepath.split('/')
    if len(path) > 1:
        return path
    return path[-1]
#path = '/mnt/c/Users/felip/Downloads/nand2tetris/nand2tetris/projects/07/MemoryAccess/PointerTest/PointerTest.vm'
path = sys.argv[1]
filepath = getFilePath(path)

file = path.split('/')[-1].split('.')[0]
code_writer = CodeWriter(filepath, file)

commands = {
    "push": code_writer.writePush,
    "pop" : code_writer.writePop,
    "add" : code_writer.writeAdd,
    "sub" : code_writer.writeSub,
    "neg" : code_writer.writeNegative,
    "not" : code_writer.writeNot,
    "and" : code_writer.writeAnd,
    "or" : code_writer.writeOr,
    "eq" : code_writer.writeEq,
    "lt" : code_writer.writeLt,
    "gt" : code_writer.writeGt,
    "function" : code_writer.writeFunction,
    "return": code_writer.writeReturn,
    "call" : code_writer.writeCall,
    "if-goto" : code_writer.writeIf,
    "label" : code_writer.writeLabel,
    "goto" : code_writer.writeGoto
}

def remove_comments(line):
    return re.match("[^//]*", line).group()

with open(path, "r") as reader:
    lines = reader.readlines()
    for line in lines:
        line = remove_comments(line)
        if line == '' or line == '\n':
            continue
        line = line.replace("\n", "")
        args = line.split()
        command = commands.get(args[0])
        if command != None : 
            command(args)
            continue
        raise(BaseException("Comando inválido"))



